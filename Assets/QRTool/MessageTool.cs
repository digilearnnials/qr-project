﻿using UnityEngine;
using UnityEngine.UI;

public class MessageTool : MonoBehaviour {

	[SerializeField] Text textMessage;
	Animator ani;

	void Awake(){
		DontDestroyOnLoad(this);
	}
	// Use this for initialization
	void Start () {
		ani = GetComponent<Animator>();
		QRTool.instance.OnQRScanEnd.AddListener(ShowMessage);
	}
	
	public void ShowMessage(string value){
		textMessage.text = value;
		ani.SetTrigger("Show");
	}
}
