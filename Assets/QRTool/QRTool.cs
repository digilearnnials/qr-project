﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using ZXing;

[System.Serializable] public class QREvent : UnityEvent<string> { }
public class QRTool : MonoBehaviour
{
    [SerializeField] RawImage camImage;
    [SerializeField] AspectRatioFitter camImageARF;
    [SerializeField] CanvasGroup qrPanel;

    public static QRTool instance;
    public QREvent OnQRScanEnd;
    private WebCamTexture camTexture;
    private Rect screenRect;
    Result result;

    [SerializeField] QRStat stat = QRStat.NONE;

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    void Update()
    {
        switch (stat)
        {
            case QRStat.NONE:
                break;
            case QRStat.START:
                StartCam();
                break;
            case QRStat.SCAN:
                ShowCam();
                break;
            case QRStat.SEND:
                ScanEnd();
                break;
        }
    }

    void StartCam()
    {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;
        if (camTexture != null)
        {
            camTexture.Play();
        }
        
        result = null;
        qrPanel.alpha = 1f;
        qrPanel.blocksRaycasts = true;
        qrPanel.interactable = true;
        stat = QRStat.SCAN;
    }

    void ShowCam()
    {
        
        if (camTexture.width < 100)
        {
            Debug.Log("Still waiting another frame for correct info...");
            return;
        }

        // change as user rotates iPhone or Android:

        int cwNeeded = camTexture.videoRotationAngle;
        // Unity helpfully returns the _clockwise_ twist needed
        // guess nobody at Unity noticed their product works in counterclockwise:
        int ccwNeeded = -cwNeeded;

        // IF the image needs to be mirrored, it seems that it
        // ALSO needs to be spun. Strange: but true.
        if (camTexture.videoVerticallyMirrored) ccwNeeded += 180;

        // you'll be using a UI RawImage, so simply spin the RectTransform
        camImage.rectTransform.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);

        float videoRatio = (float)camTexture.width / (float)camTexture.height;

        // you'll be using an AspectRatioFitter on the Image, so simply set it
        camImageARF.aspectRatio = videoRatio;

        // alert, the ONLY way to mirror a RAW image, is, the uvRect.
        // changing the scale is completely broken.
        if (camTexture.videoVerticallyMirrored)
            camImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
        else
            camImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip

        camImage.texture = camTexture;
    }

    public void ScanProcess()
    {
        // do the reading — you might want to attempt to read less often than you draw on the screen for performance sake
        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                stat = QRStat.SEND;
            }
        }
        catch (Exception ex)
        {
            Debug.LogWarning(ex.Message);
            stat = QRStat.SEND;
        }
    }

    void ScanEnd()
    {
        if (camTexture != null)
        {
            camTexture.Stop();
        }
        qrPanel.alpha = 0f;
        qrPanel.blocksRaycasts = false;
        qrPanel.interactable = false;

        if (result != null)
        {
            OnQRScanEnd.Invoke(result.Text);
        }
        stat = QRStat.NONE;
    }

    public void SetQRStat(QRStat value = QRStat.NONE)
    {
        stat = value;
    }
}

public enum QRStat
{
    NONE,
    START,
    SCAN,
    SEND
}
