﻿using UnityEngine;

public class ColorControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		QRTool.instance.OnQRScanEnd.AddListener(ChangeColor);
	}

	public void ChangeColor(string value){

		Color color = Color.white;

		ColorUtility.TryParseHtmlString(value, out color);

		//Fetch the Renderer from the GameObject
        Renderer rend = GetComponent<Renderer>();

        //Set the main Color of the Material to green
        rend.material.color = color;
	}
	
	public void CallQRScan(){
		QRTool.instance.SetQRStat(QRStat.START);
	}
}
